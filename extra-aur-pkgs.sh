#!/usr/bin/env bash
#-------------------------------------------------------------------------
echo -ne "


██╗  ██╗ ██████╗ ████████╗████████╗███████╗███████╗██╗    ██╗ █████╗ ██████╗  █████╗ ███╗   ██╗
██║ ██╔╝██╔═══██╗╚══██╔══╝╚══██╔══╝██╔════╝██╔════╝██║    ██║██╔══██╗██╔══██╗██╔══██╗████╗  ██║
█████╔╝ ██║   ██║   ██║      ██║   █████╗  ███████╗██║ █╗ ██║███████║██████╔╝███████║██╔██╗ ██║
██╔═██╗ ██║   ██║   ██║      ██║   ██╔══╝  ╚════██║██║███╗██║██╔══██║██╔══██╗██╔══██║██║╚██╗██║
██║  ██╗╚██████╔╝   ██║      ██║   ███████╗███████║╚███╔███╔╝██║  ██║██║  ██║██║  ██║██║ ╚████║
╚═╝  ╚═╝ ╚═════╝    ╚═╝      ╚═╝   ╚══════╝╚══════╝ ╚══╝╚══╝ ╚═╝  ╚═╝╚═╝  ╚═╝╚═╝  ╚═╝╚═╝  ╚═══╝

#  Arch Linux Post Install Setup and Config
#-------------------------------------------------------------------------
"
echo
echo "INSTALLING AUR SOFTWARE"
echo

cd "${HOME}"

echo "CLOING: PARU"
git clone "https://aur.archlinux.org/paru.git"

PKGS=(
'anydesk'
'hunspell-en-med-glut-git'
'wps-office'
'times-newer-roman'
'ttf-wps-fonts'
'stacer-bin'
'ventoy-bin'
'kwin-scripts-forceblur'
'vimix-cursors'
'kwin-scripts-window-colors'
'whitesur-kde-theme-git'
'whatsdesk-bin'
'noisetorch'
'Aspell-ta'
'bamini-tamil-font'
'aic94xx-firmware'
'ast-firmware'
'wd719x-firmware'
'upd72020x-fw'
'mkinitcpio-firmware'
'libtiff5'
'preload'
'auto-cpufreq'
'ttf-ms-fonts'
'linux-xanmod-edge-linux-bin-x64v3'
)

cd ${HOME}/paru
makepkg -si

for PKG in "${PKGS[@]}"; do
    echo "INSTALLING: ${PKG}"
    paru -S "$PKG" --noconfirm --needed
done


echo
echo "Done!"
echo














