#!/bin/bash
# Installing WhiteSur-kde.git

echo "Installing WhiteSur-kde.git."

echo "Cloning the WhiteSur-kde.git"

cd ~
git clone "https://github.com/vinceliuice/WhiteSur-kde.git"

echo "Executing WhiteSur-kde.git"

cd $HOME/WhiteSur-kde

exec ./install.sh


